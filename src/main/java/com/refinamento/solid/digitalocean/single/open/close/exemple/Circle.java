package com.refinamento.solid.digitalocean.single.open.close.exemple;

public class Circle {
    int r;
    int area() {
        return (int) (Math.PI * r * r);
    }
}
