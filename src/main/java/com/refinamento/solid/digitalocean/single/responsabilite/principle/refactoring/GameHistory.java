package com.refinamento.solid.digitalocean.single.responsabilite.principle.refactoring;

import com.refinamento.solid.digitalocean.single.responsabilite.principle.GameResult;

import java.util.ArrayList;
import java.util.List;

public class GameHistory {
    private static List<GameResult> history = new ArrayList();

    public static void printHistory(){ /* lots of code */ }
    public static void updateRow()   { /* lots of code */ }
    public static void deleteRow()   { /* lots of code */ }
    public static void editRow()     { /* lots of code */ }
}
