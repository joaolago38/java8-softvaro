package com.refinamento.geek.java8.exemple;

import java.util.Arrays;

public class GFG4 {
    public static void main(String args[])
    {
        // Input string
        String str = "HelloGeeks";
        // Character array using toCharArray() method
        char[] char_array = str.toCharArray();
        // printing the char_array
        System.out.println(Arrays.toString(char_array));

    }
}
