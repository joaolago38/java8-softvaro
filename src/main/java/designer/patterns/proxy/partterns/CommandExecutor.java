package designer.patterns.proxy.partterns;

public interface CommandExecutor {
    public void runCommand(String cmd) throws Exception;
}
