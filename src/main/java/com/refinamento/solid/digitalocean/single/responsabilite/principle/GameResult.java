package com.refinamento.solid.digitalocean.single.responsabilite.principle;

public class GameResult {
    int guesses;
    int magicNumber;

    GameResult( int numberOfGuesses, int theNumber) {
        guesses = numberOfGuesses;
        magicNumber = theNumber;
    }
}
