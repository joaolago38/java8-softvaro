package com.refinamento.solid.digitalocean.single.open.close.exemple;

public class Square {
    int height;
    int area() { return height * height; }
}
