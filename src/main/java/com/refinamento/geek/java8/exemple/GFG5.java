package com.refinamento.geek.java8.exemple;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;

public class GFG5 {
    public static void main(String args[]) throws IOException {
        // String
        String str = "HelloGeeks";

        // length of string
        int len = str.length();

        // character array
        char[] char_array = new char[len];

        // StringReader class object
        StringReader reader = new StringReader(str);

        // integer variable
        int int_var;

        int current_index = 0;
        // taking current character into integer variable
        // from StringReader object until returned variable
        // is not equal to -1 (-1 denotes end of string)
        while ((int_var = reader.read()) != -1)
        {
            char_array[current_index] = (char)(int_var);
            current_index++;
        }

        System.out.println(Arrays.toString(char_array));
    }
}
