package com.refinamento.geek.java8.exemple;

import java.util.Arrays;

public class GFG3 {
    public static void main(String args[])
    {
        // Input string
        String str = "HelloGeeks";

        // Length of string
        int len = str.length();

        // Character array of string length
        char[] char_array = new char[len];

        // Looping and accessing each character
        for (int i = 0; i < len; i++)
        {
            // assigning the current character to ith index
            // of char array
            char_array[i] = str.charAt(i);
        }

        // printing the char_array
        System.out.println(Arrays.toString(char_array));
    }
}
