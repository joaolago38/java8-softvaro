package com.refinamento.solid.digitalocean.single.open.close.exemple;

public class OpenOpenExample {
    public int compareArea(Square a, Square b) {
        return a.area() - b.area();
    }

    public int compareArea(Circle x, Circle y) {
        return x.area() - y.area();
    }
}
