package designer.patterns.creations.abstr.factory;

import designer.patterns.creations.factory.Server;

public class ComputerFactory {
    public static Computer getComputer(ComputerAbstractFactory factory){
        return factory.createComputer();
    }
}
