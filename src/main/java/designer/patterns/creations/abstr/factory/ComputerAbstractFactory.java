package designer.patterns.creations.abstr.factory;

public interface ComputerAbstractFactory {
    public Computer createComputer();
}
