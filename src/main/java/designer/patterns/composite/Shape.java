package designer.patterns.composite;

public interface Shape {
    public void draw(String fillColor);
}
