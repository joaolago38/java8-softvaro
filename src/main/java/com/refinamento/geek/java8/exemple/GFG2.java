package com.refinamento.geek.java8.exemple;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class GFG2 {
    // Generic function to convert List of
    // String to List of Integer
    public static <K, V> Stream<V>
    convertMapToStream(Map<K, V> map)
    {

        // Return the obtained Stream
        return map

                // Convert the Map to Set<Value>
                .values()

                // Convert the Set to Stream
                .stream();
    }
    public static void main(String args[])
    {
        // Create a Map
        Map<Integer, String> map = new HashMap<>();

        // Add entries to the Map
        map.put(1, "Geeks");
        map.put(2, "forGeeks");
        map.put(3, "A computer Portal");

        // Print the Map
        System.out.println("Map: " + map);

        // Convert the Map to Stream
        Stream<String> stream = convertMapToStream(map);

        // Print the TreeMap
        System.out.println("Stream: "
                + Arrays.toString(stream.toArray()));
    }
}
